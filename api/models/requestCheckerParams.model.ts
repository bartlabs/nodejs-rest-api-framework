export class FwRequestCheckerParamsModel {
    exactLength?: number;

    example?: string;

    minLength?: number;

    maxLength?: number;

    onlyLetters?: boolean;

    required?: boolean;

    type?: boolean | number | string;
}
