/**
 * @swagger
 * definition:
 *   message:
 *     properties:
 *       message:
 *         type: string
 */

export class FwMessageResponseModel {
    // tslint:disable-next-line: member-access
    message: string;
}
