/**
 * @swagger
 * definition:
 *   error:
 *     properties:
 *       error:
 *         type: string
 *       catched:
 *         type: boolean
 */

export class FwErrorResponseModel {
    error: string;

    type: boolean;
}
