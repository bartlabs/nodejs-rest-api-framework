/**
 * @swagger
 * definition:
 *   token:
 *     properties:
 *       expiration:
 *         type: number
 *         description: Time (NumericDate) when token will expire
 *       token:
 *         type: string
 *         description: Access token
 *     required:
 *      - expiration
 *      - token
 */

export class FwTokenModel {
    expiration: Date;

    token: string;
}
