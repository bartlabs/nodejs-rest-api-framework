/**
 * @swagger
 * definition:
 *   login:
 *     properties:
 *       hash:
 *         type: number
 *         description: Generated user hash
 *       username:
 *         type: string
 *         description: Unique username
 *       timestamp:
 *         type: string
 *         description: Current unix timestamp
 *     required:
 *      - hash
 *      - username
 *      - timestamp
 */

export class FwLoginModel {
    hash: string;

    username: string;

    timestamp: number;
}
