export class FwEnvModel {
    cryptoIv: string;

    host: string;

    jwtKey: string;

    nodeEnv: string;

    port: number | string;
}
