export class FwModuleResponseModel {
    body: any;

    initData?: any; // Api Initial data - before mapping (body)

    status: boolean;
}
