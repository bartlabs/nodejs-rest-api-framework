import dotenv from 'dotenv';

import { FwEnvModel } from '@fw/models/env.model';

dotenv.config();

const cryptoIv = process.env.CRYPTO_IV;
const host = process.env.HOST;
const jwtKey = process.env.JWT_KEY;
const nodeEnv = process.env.NODE_ENV || 'dev'; // <'dev' | 'test' | 'stage' | 'prod'>
const port = process.env.PORT;

export const ENV: FwEnvModel = {
    cryptoIv,
    host,
    jwtKey,
    nodeEnv,
    port,
};
