import {
    FwHttpMethodsEnum as Method, FwParamLocationsEnum as Location, FwRoutesParamNamesEnum as Param,
} from '@fw/enums';
import { FwRoutesType } from '@fw/types';

// EXEMPLARY ROUTES_CONFIG
// tslint:disable: object-literal-sort-keys
export const ROUTES_CONFIG: FwRoutesType = {
    '/test/{chars}': {
        methods: {
            [Method.GET]: {
                [Location.PATH]: {
                    [Param.chars]: {
                        required: true,
                        type: 'string',
                        onlyLetters: true,
                        minLength: 3,
                        maxLength: 50,
                    },
                },
            },
        },
    },
};
