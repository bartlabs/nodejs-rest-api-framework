import { ENV } from '@fw/env';

const host = `${ENV.host}:${ENV.port}`;

export let SWAGGER_DEFAULT_OPTIONS: any = { // TODO - type?
    apis: [
        './api/**/index.ts',
        './api/**/models/**/*.ts',
        './fw/api/models/**/*.ts', // Don't remove this path
      ],
      swaggerDefinition: {
        basePath: '/',
        components: {
          securitySchemes: {
            bearerAuth: {
              bearerFormat: 'JWT',
              scheme: 'bearer',
              type: 'http',
            },
          },
        },
        host,
        info: {
          contact: {
            email: '<email>',
          },
          title: '<title>',
          version: '1.0.0',
        },
        produces: 'application/json',
        responses: {
          // tslint:disable-next-line: object-literal-key-quotes
          '400': {
            description: 'Invalid parameters.',
            schema: { $ref: '#/definitions/message' },
          },
          // tslint:disable-next-line: object-literal-key-quotes
          '401': {
            description: 'Authentication failed.',
            schema: { $ref: '#/definitions/message' },
          },
          // tslint:disable-next-line: object-literal-key-quotes
          '500': {
            description: 'Unexpected error.',
            schema: { $ref: '#/definitions/error' },
          },
        },
        security: [{
          bearerAuth: { },
        }],
        // schemes: ["https","http"],
        // openapi: "3.0.0", // TODO - obadać
      },
};
