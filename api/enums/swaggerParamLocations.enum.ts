export enum FwParamLocationsEnum {
    BODY = 'body',
    COOKIE = 'cookie',
    HEADER = 'header',
    PATH = 'path',
    QUERY = 'query',
}
