export enum FwHttpMethodsEnum {
    GET = 'get',
    POST = 'post',
    PUT = 'put',
    DELETE = 'delete',
    PATCH = 'patch',
    HEAD = 'head',
    TRACE = 'trace',
    OPTIONS = 'options',
    CONNECT = 'connect',
}

export const FwHttpMethodsLabels = [
    {
        label: 'get',
        value: FwHttpMethodsEnum.GET,
    },
    {
        label: 'post',
        value: FwHttpMethodsEnum.POST,
    },

    // TODO others...
];
