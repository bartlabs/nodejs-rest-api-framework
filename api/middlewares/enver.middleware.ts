import { NextFunction, Request, Response } from 'express';
import isNil from 'lodash/isNil';

import { FwResponder } from '@fw/controllers';
import { ENV } from '@fw/env';

class FwEnverMiddeleware {

    checker = (req: Request, res: Response, next: NextFunction) => {
        try {
            this._checkIfIsComplete(res) && next();

            // If there will be more checkers:
            // if (!res.statusCode || res.statusCode === 200) {
            //     next();
            // }
        } catch (e) {
            return FwResponder.errorCatcher(res, e, true);
        }
    }

    private _checkIfIsComplete(res: Response) {
        try {
            for (const prop of Object.values(ENV)) {
                if (isNil(prop)) {
                    FwResponder.errorCatcher(res, '.env file is incomplete - fill the missing data.');
                    return false;
                }
            }

            return true;
        } catch (e) {
            FwResponder.errorCatcher(res, e, true);
            return false;
        }
    }
}

export default FwEnverMiddeleware;
