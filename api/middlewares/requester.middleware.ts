import { NextFunction, Request, Response } from 'express';
import { cloneDeep, forEach, get, keys, startsWith, toLower } from 'lodash';
import url from 'url';

import { ROUTES_CONFIG } from '@fw/config';
import { FwResponder } from '@fw/controllers';
import { FwHttpMethodsEnum, FwParamLocationsEnum } from '@fw/enums';
import { FwMessageResponseModel, FwRequestCheckerParamsModel } from '@fw/models';
import { FwRoutesType } from '@fw/types';

class FwRequesterMiddeleware {
    defaultParams: FwRequestCheckerParamsModel;
    // tslint:disable-next-line: array-type
    params: Array<Array<FwRequestCheckerParamsModel>>;
    routes: FwRoutesType = ROUTES_CONFIG; // Deafult fw sample ROUTES_CONFIG

    private _in: FwParamLocationsEnum;
    private _isFailed: boolean;
    private _method: FwHttpMethodsEnum;
    private _param: string;
    private _paramName: string;
    private _response: Response;
    private _request: Request;

    constructor(routes: FwRoutesType) {
        routes && (this.routes = routes);
    }

    checker = (req: Request, res: Response, next: NextFunction) => {
        try {
            this._isFailed = false;
            this._paramName = null;
            this._response = res;
            this._request = req;

            this._checkParams();

            if (!this._response.statusCode || this._response.statusCode === 200) {
                // this._queryCapture();
                next();
            }
        } catch (e) {
            return FwResponder.errorCatcher(res, e, true);
        }
    }

    private _checkParams() {
        try {
            const currentPath = url.parse(this._request.url).pathname;

            forEach(this.routes, async (methods, path) => {
                const cleanPath = path.substr(0, path.indexOf('/{'));

                if (cleanPath && startsWith(currentPath, cleanPath)) {
                    this._method = get(FwHttpMethodsEnum, (this._request.method).toUpperCase());
                    const paramLocations = get(methods.methods, this._method);

                    forEach(paramLocations, (checkerParams, swaggerIn) => {
                        this._in = get(FwParamLocationsEnum, swaggerIn.toUpperCase());

                        if (checkerParams && keys(checkerParams).length) {
                            forEach(checkerParams, (paramsList: FwRequestCheckerParamsModel, paramName) => {
                                if (this._isFailed) {
                                    return false;
                                }

                                this._setRequestParam(paramName);

                                if (this._param) {
                                    paramsList.minLength
                                        && this._minLength(paramsList.minLength);

                                    paramsList.maxLength
                                        && this._maxLength(paramsList.maxLength);

                                    paramsList.exactLength
                                        && this._exactLength(paramsList.exactLength);

                                    paramsList.onlyLetters
                                        && this._onlyLetters();

                                } else if (paramsList.required) {
                                    this._failedMessage(`is required`);
                                }
                            });
                        }
                    });
                }
            });

        } catch (e) {
            return FwResponder.errorCatcher(this._response, e, true);
        }
    }

    // Additional query string parameters - TODO????
    // private _queryCapture() {
    //     try {
    //         const queryResultType = toLower(this._request.query.r) || false;
    //         const queryPassword = this._request.query.p || false;
    //         const apiPass = process.env.API_TMP_PASS || false;

    //         if (apiPass && apiPass === queryPassword && queryResultType === 'initdata') {
    //             this._response.locals.initdata = true;
    //         }
    //     } catch (e) {
    //         return FwResponder.errorCatcher(this._response, e, true);
    //     }
    // }

    private _setRequestParam(paramName: string) {
        paramName && (this._paramName = paramName);
        let params: any[any];

        switch (this._in) {
            case FwParamLocationsEnum.BODY:
                params = this._request.body;
                break;
            // TODO - something else?

            default:
                params = this._request.params;
        }

        this._param = params[paramName] || null;
    }

    // public setOnlyLetters(req: Request, res: Response, next: NextFunction) {
    //     res.locals.queryString.onlyLetters = true;
    //     next();
    // }

    private _exactLength(length: number) {
        if (this._param.length !== length) {
            this._failedMessage(`must contain exactly ${length} characters`);
        }
    }

    private _minLength(length: number) {
        if (this._param.length < length) {
            this._failedMessage(`contains too few characters (min: ${length})`);
        }
    }

    private _maxLength(length: number) {
        if (this._param.length > length) {
            this._failedMessage(`contains too many characters (max: ${length})`);
        }
    }

    private _onlyLetters() {
        const onlyLetters = /^[AaĄąBbCcĆćDdEeĘęFfGgHhIiJjKkLlŁłMmNnŃńOoÓóPpRrSsŚśTtUuWwYyZzŹźŻż]+$/;
        if (!this._param.match(onlyLetters)) {
            this._failedMessage('can contain only letters');
        }
    }

    private _failedMessage(text: string) {
        const message = `${this._in.toUpperCase()} parameter {${this._paramName}} ${text}.`;
        const messageResponse: FwMessageResponseModel = { message };
        this._response.status(400).send(messageResponse);
        this._isFailed = true;
    }
}

export default FwRequesterMiddeleware;
