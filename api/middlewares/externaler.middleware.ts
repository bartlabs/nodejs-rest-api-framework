import { NextFunction, Request, Response } from 'express';
import { get, isNil, omitBy } from 'lodash';
import fetch from 'node-fetch';

import { EXTERNAL_URLS_CONFIG } from '@fw/config';
import { FwResponder } from '@fw/controllers';
import { FwHttpMethodsEnum } from '@fw/enums';
import { FwModuleResponseModel } from '@fw/models';

class FwExternalerMiddeleware {
    externalUrls = EXTERNAL_URLS_CONFIG; // deafult fw sample EXTERNAL_URLS_CONFIG

    constructor(externalUrls: any) {
        externalUrls && (this.externalUrls = externalUrls);
    }

    init = (req: Request, res: Response, next: NextFunction) => {
        try {
            const externalUrl = req.route.path && get(this.externalUrls, req.route.path);

            if (externalUrl && req.method) {
                const method: FwHttpMethodsEnum = get(FwHttpMethodsEnum, (req.method).toUpperCase());
                const body = Object.keys(req.body).length ? req.body : null;

                const fetchParams = omitBy({
                    body,
                    method,
                }, isNil);

                fetch(externalUrl, fetchParams)
                .then((fetchResponse: any) => fetchResponse.json())
                .then((fetchJson: any) => {
                    const response: FwModuleResponseModel = {
                        body: fetchJson,
                        status: !!fetchJson,
                    };
                    // tslint:disable-next-line: no-string-literal
                    res['external_body'] = response;

                    next();
                })
                .catch((err: any) => {
                    return FwResponder.errorCatcher(res, 'Fetch external URL error: ' + err, true);
                });
            } else {
                return FwResponder.errorCatcher(res, 'Fetch external URL error: NO ROUTING FOR THIS URL', false);
            }
        } catch (err) {
            return FwResponder.errorCatcher(res, err, true);
        }
    }
}

export default FwExternalerMiddeleware;
