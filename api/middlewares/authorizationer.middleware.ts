import crypto from 'crypto';
import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import find from 'lodash/find';

import { AUTHORIZED_USERS } from '@fw/config';
import { ENV } from '@fw/env';
import { FwAuthorizeUserModel } from '@fw/models';

// const jwt = require('jsonwebtoken');
// const crypto = require('crypto');

const cryptoAlgorithm = 'aes-256-cbc';
const cryptoKey = crypto.randomBytes(32);
const cryptoIv = ENV.cryptoIv;
const expirationTime = 60; // [min] Signing a token with 1 hour of expiration
const jwtSecretKey = ENV.jwtKey || false;

class FwAuthorizationerMiddeleware {
    authorizedUsers: FwAuthorizeUserModel[] = AUTHORIZED_USERS; // deafult and fw sample AUTHORIZED_USERS

    constructor(authorizedUsers: FwAuthorizeUserModel[]) {
        authorizedUsers && (this.authorizedUsers = authorizedUsers);
    }

    // TODO - add logs of successes / failures??

    verify = (req: Request, res: Response, next: NextFunction) => {
        try {
            if (this._checkJwtKey(res)) {
                return res; // respond msg if JWT KEY doesn't exist
            }

            // Format token: Bearer <access_token>;
            const authHeader = req.headers.authorization.split(' ');
            if (typeof authHeader !== 'undefined' && authHeader[1]) {
                const tokenDecrypted = this._decrypt(authHeader[1]);
                if (tokenDecrypted) {
                    const decoded = jwt.verify(tokenDecrypted, jwtSecretKey);
                    // req.userData = decoded; // TODO ????
                    next();
                } else {
                    return res.status(401).json({
                        message: 'Invalid authentication token.',
                    });
                }
            } else {
                return res.status(401).json({
                    message: 'The request did not include an authentication token.',
                });
            }
        } catch (error) {
            return res.status(401).json({
                message: 'The authentication credentials are missing, or if supplied are not valid or not sufficient to access the resource.',
            });
        }
    }

    sign = (req: Request, res: Response, next: NextFunction) => {
        try {
            if (this._checkJwtKey(res)) {
                return res; // respond msg if JWT KEY doesn't exist
            }

            const verifyBodyParams = this._verifyBodyParams(req.body);
            if (verifyBodyParams === true) {
                const exp = Math.floor(Date.now() / 1000) + (60 * expirationTime); // Number of seconds since the epoch.
                const { username, hash } = req.body;
                username && hash && jwt.sign({ username, hash, exp }, jwtSecretKey, (err, token) => {
                    if (err) {
                        return res.status(500).json({
                            message: 'Token generation failed - sign error.',
                        });
                    }
                    if (token) {
                        const tokenEncrypted = this._encrypt(encodeURI(token));
                        if (tokenEncrypted) {
                            res.json({
                                expiration: exp,
                                token: tokenEncrypted,
                             });
                        } else {
                            return res.status(500).json({
                                message: 'Token generation failed - encrypt error.',
                            });
                        }
                    } else {
                        return res.status(500).json({
                            message: 'Token generation failed - no result.',
                        });
                    }
                });

            } else {
                const message = verifyBodyParams || 'The authentication credentials are missing, or if supplied are not valid or not sufficient to access the resource.';
                return res.status(401).json({ message });
            }
        } catch (error) {
            // tslint:disable-next-line: no-console
            console.log(error);
            return res.status(500).json({
                message: 'Token generation failed - catch error.',
            });
        }
    }

    getUserToken(username: string, password: string) {
        const userPass = this._getAuthorizedUserPass(username);

        if (userPass === password) {
            const timestamp = Date.now().toString();
            const hash = crypto.createHash('sha256')
                .update(userPass.replace('{STAMP}', timestamp))
                .digest('hex');

            return { username, hash, timestamp };
        }

        return false;
    }

    private _checkJwtKey(res: Response) {
        if (!jwtSecretKey) {
            // tslint:disable-next-line: no-console
            console.log('ALERT - add JWT_KEY to .env');
            return res.status(500).json({
                message: 'Token generation failed - no key error.',
            });
        }
    }

    private _getAuthorizedUserPass(username: string): string {
        const authorizedUser = find(this.authorizedUsers, { username });
        return authorizedUser && authorizedUser.pass || undefined;
    }

    private _verifyBodyParams(reqBody) {
        if (reqBody &&
            reqBody.username && reqBody.timestamp && reqBody.hash) { // 1 - check all params
            const userPass = this._getAuthorizedUserPass(reqBody.username);

            if (userPass) { // 2 - username match
                const userTimestamp = Number(reqBody.timestamp);

                // 3 - timestamp match
                const currentTimestamp = Date.now();
                const maxDelay = 60000; // max 1 min (60ms)
                const delay = currentTimestamp - userTimestamp;
                if (delay >= 0 && delay <= maxDelay) {
                    // 4 - hashes match
                    const hash = crypto.createHash('sha256')
                        .update(userPass.replace('{STAMP}', reqBody.timestamp))
                        .digest('hex');

                    if (hash === reqBody.hash) {
                        return true;
                    }
                } else {
                    return 'Timestamp doesn\'t match the current time';
                }
            }
        }

        return false;
    }

    private _encrypt(text) {
        try {
            const cipher = crypto.createCipheriv(cryptoAlgorithm, Buffer.from(cryptoKey), Buffer.from(cryptoIv));
            let encrypted = cipher.update(text);
            encrypted = Buffer.concat([encrypted, cipher.final()]);
            return encrypted.toString('hex');
        } catch (error) {
            return false;
        }
    }

    private _decrypt(text) {
        try {
            const encryptedText = Buffer.from(text, 'hex');
            const decipher = crypto.createDecipheriv(cryptoAlgorithm, Buffer.from(cryptoKey), Buffer.from(cryptoIv));
            let decrypted = decipher.update(encryptedText);
            decrypted = Buffer.concat([decrypted, decipher.final()]);
            return decrypted.toString();
        } catch (error) {
            return false;
        }
    }
}

export default FwAuthorizationerMiddeleware;
