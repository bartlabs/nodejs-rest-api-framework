import { FwRoutesType } from '@fw/types';
import { assign, map } from 'lodash';

class FwRoutesController {
    routes: FwRoutesType;

    constructor() {
        map(this.routes, (routes, path) => {
            assign(routes, { path });
        });
    }

    init() {
        return this.routes;
    }
}

export default new FwRoutesController().init();
