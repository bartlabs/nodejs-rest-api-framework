import { FwModuleResponseModel } from '@fw/models';
import { Response } from 'express';

class FwResponderController {
    errorCatcher(res: Response, error: any, catched = false) {

        let errorText = error || '';

        if (error instanceof Error) {
            errorText = (error.name || 'Undefined error )-') + ': ' + (error.message || '');
        }

        errorText = String(errorText) || 'Unexpected error ;-(';

        res.status(500);
        res.send({ error: errorText, catched });
    }

    moduleResponder(res: Response, moduleResponse: FwModuleResponseModel) {
        if (moduleResponse && moduleResponse.status && moduleResponse.body) {
            if (res.locals.initdata && moduleResponse.initData) {
                // Conflict
                // - the request couldn't be completed due to a conflict with the current state of the target resource
                res.status(409);
                res.send(moduleResponse.initData);
            } else {
                res.send(moduleResponse.body);
            }
        } else {
            const error = moduleResponse.body || 'No module FwResponder';
            this.errorCatcher(res, error, true);
        }
    }
}

export default new FwResponderController();
