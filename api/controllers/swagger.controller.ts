import express from 'express';
import { cloneDeep, find, forEach, get, isBoolean, isNumber, isString, map, set } from 'lodash';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

import { ROUTES_CONFIG, SWAGGER_DEFAULT_OPTIONS } from '@fw/config';
import { FwParamLocationsEnum } from '@fw/enums';
import { FwRequestCheckerParamsModel } from '@fw/models';
import { FwRoutesType } from '@fw/types';

// tslint:disable-next-line: no-var-requires
const zSchemaJSon = require('z-schema/src/schemas/schema.json');

class FwSwaggerController {
  private _app: express.Express;
  private _routes: FwRoutesType = ROUTES_CONFIG; // deafult and fw sample ROUTES_CONFIG
  private _swaggerOptions = SWAGGER_DEFAULT_OPTIONS; // deafult and fw sample SWAGGER_DEFAULT_OPTIONS
  private _swaggerSpec;

  constructor(
    app: express.Express,
    routes: FwRoutesType,
    swaggerOptions?: any,
  ) {
    this._app = app;
    this._routes = routes;

    swaggerOptions && (this._swaggerOptions = swaggerOptions);
    this._swaggerSpec = swaggerJSDoc(this._swaggerOptions);
  }

  init = () => {
    this._setRouteParams();

    this._app.get('/swagger.json', (req, res) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(this._swaggerSpec);
    });

    const customOptions = {
      customCss:
        '.parameters-col_description .restrictions { float: right } ' +
        '.parameters-col_description code { display: block; float: left; margin: 0 4px 4px 4px } ' +
        '.parameters-col_description input:disabled { display: none } ',
    };

    this._app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(this._swaggerSpec, customOptions));
  }

  private _insertDefaultErrorsResponses(currentResponses: any[string]): any[string] {
    forEach(this._swaggerOptions.swaggerDefinition.responses, (ref, httpCode) => {
      if (currentResponses && !currentResponses[httpCode]) {
        currentResponses[httpCode] = ref;
      }
    });

    return currentResponses;
  }

  private _insertRestrictionsIntoDescription(params: FwRequestCheckerParamsModel) {
    let restrictions = '';
    map(params, (param, key) => {
      // Only number, string and boolean is allowed
      if (isNumber(param) || isString(param)) {
        restrictions += ` \`${key}: ${param}\` `;
      } else if (isBoolean(param)) {
        restrictions += ` \`${key}\` `;
      }
    });

    return `<span class="restrictions">${restrictions}</span>`;
  }

  private _setSchemaProperties(params: FwRequestCheckerParamsModel, schema: any[any]) {
    const newSchema = cloneDeep(schema);
    map(params, (param, key) => {
      // node_modules\z-schema\src\schemas\schema.json
      if (zSchemaJSon.properties[key]) {
        newSchema[key] = param;
      }
    });

    return newSchema;
  }

  private _insertRestrictionsIntoSchema(
    paramName: string, paramLocation: FwParamLocationsEnum, params: FwRequestCheckerParamsModel, schema: any[any],
  ) {
    switch (paramLocation) {
      case FwParamLocationsEnum.PATH:
        // schema: <FwRequestCheckerParamsModel>
        schema = this._setSchemaProperties(params, schema);
        !schema.example && (schema.example = ' ');
        break;

      case FwParamLocationsEnum.BODY:
        // schema: { properties: {<paramName>: <FwRequestCheckerParamsModel>}, required: [<paramName>]}
        schema = set(
          schema,
          ['properties', paramName],
          this._setSchemaProperties(params, schema),
        );

        if (schema.properties[paramName].required) {
          schema = set(
            schema,
            ['required'],
            [paramName],
          );

          delete schema.properties[paramName].required;
        }
        break;
    }

    return schema;
  }

  private _setRouteParams() {
    forEach(this._routes, (route, path) => {
      forEach(route.methods, (paramLocations, method) => {
        forEach(paramLocations, (paramNames, swaggerIn) => {
          forEach(paramNames, (params, paramName) => {
            const specPathParametersList = get(this._swaggerSpec.paths, [path, method, 'parameters']);
            const specPathParametersItem = find(specPathParametersList, (param) => (param.name === paramName));

            if (specPathParametersItem) {
              // Set restrictions tags into description
              const description = specPathParametersItem.description || '';
              specPathParametersItem.description = description + this._insertRestrictionsIntoDescription(params);

              // Set restrictions params into schema
              const schema = specPathParametersItem.schema || {};
              const paramLocation = get(FwParamLocationsEnum, swaggerIn.toUpperCase());
              specPathParametersItem.schema =
                this._insertRestrictionsIntoSchema(paramName, paramLocation, params, schema);

              // Set required flag
              specPathParametersItem.required = params.required || false;
            }
          });
        });

        // Set default errors responses
        let pathResponsesList = get(this._swaggerSpec.paths, [path, method, 'responses']);
        pathResponsesList = this._insertDefaultErrorsResponses(pathResponsesList);
      });
    });

    const swaggerSpecStr = JSON.stringify(this._swaggerSpec);
    this._swaggerSpec = JSON.parse(swaggerSpecStr);
  }
}

export default FwSwaggerController;
