import FwResponderController from './responder.controller';
import FwRoutesController from './routes.controller';
import FwSwaggerController from './swagger.controller';

export {
    FwResponderController as FwResponder,
    FwRoutesController as FwRoutes,
    FwSwaggerController as FwSwagger,
};
