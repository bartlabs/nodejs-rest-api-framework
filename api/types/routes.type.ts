import {
    FwHttpMethodsEnum as Method, FwParamLocationsEnum as Location, FwRoutesParamNamesEnum as Param,
} from '@fw/enums';
import { FwRequestCheckerParamsModel } from '@fw/models';

// Add this to your API:
// type FwRoutesType = {}; // Fake overvite type

// tslint:disable: no-shadowed-variable interface-over-type-literal
export type FwRoutesType = {
    [key: string]: {
        [key in 'methods' | 'path']?: {
            [key in Method]?: {
                [key in Location]?: {
                    [key in Param]?: FwRequestCheckerParamsModel
                }
            }
        }
    },
};
